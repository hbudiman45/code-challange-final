import React, { Component } from "react";
import { Card, CardImg, CardBody, CardTitle, CardSubtitle } from "reactstrap";

class CardList extends Component {
  render() {
    return (
      <Card style={{ marginTop: "15px" }}>
        <CardBody>
          <CardImg top width="30%" src={this.props.src} alt="Card image cap" />{" "}
          <CardTitle>Author: {this.props.title}</CardTitle>
          <CardSubtitle>Email: {this.props.subtitle}</CardSubtitle>
        </CardBody>
      </Card>
    );
  }
}

export default CardList;

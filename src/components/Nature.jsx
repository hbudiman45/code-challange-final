import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import CardList from "./Card";

class Nature extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: []
    };
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users")
      // fetch("http://picsum.photos/list")
      .then(response => response.json())
      .then(data => this.setState({ photos: data }));
  }

  render() {
    const { photos } = this.state;
    console.log("data: ", photos);
    return (
      <Container>
        <Row>
          {photos.map((photo, index) => (
            <Col md="3" key={index}>
              <CardList
                src="http://picsum.photos/300/?random"
                title={photo.name}
                subtitle={photo.email}
              />
            </Col>
          ))}
        </Row>
      </Container>
    );
  }
}

export default Nature;

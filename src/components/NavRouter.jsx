import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./MainContent";
import Tech from "./Tech";
import Nature from "./Nature";
import City from "./City";

class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Route exact path="/" component={Home} />
          <Route exact path="/tech" component={Tech} />
          <Route exact path="/nature" component={Nature} />
          <Route exact path="/city" component={City} />
        </div>
      </BrowserRouter>
    );
  }
}

export default Router;

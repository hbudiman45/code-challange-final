import React, { Component } from "react";
import { Navbar, NavbarBrand } from "reactstrap";

class Footer extends Component {
  render() {
    return (
      <Navbar color="danger">
        <NavbarBrand>footer</NavbarBrand>
      </Navbar>
    );
  }
}

export default Footer;
